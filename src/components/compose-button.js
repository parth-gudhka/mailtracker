/**
 * Created by kunal on 18-02-2017.
 */

import React, { Component } from 'react';

export default class ComposeButton extends Component{
    render(){
        return(
            <div className="compose-button"> Compose </div>
        );
    }
}