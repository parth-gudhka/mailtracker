import React, { Component } from 'react';
import Dashboard from './dashboard/dashboard';
import ComposeButton from './compose-button';

export default class App extends Component {
  render() {
    return (
      <div>
        <Dashboard />
        <ComposeButton />
      </div>
    );
  }
}
